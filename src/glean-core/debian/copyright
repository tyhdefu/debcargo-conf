Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: glean-core
Upstream-Contact:
 Jan-Erik Rediger <jrediger@mozilla.com>
 The Glean Team <glean-team@mozilla.com>
Source: https://github.com/mozilla/glean

Files: *
Copyright:
 2019-2021 Jan-Erik Rediger <jrediger@mozilla.com>
 2019-2021 The Glean Team <glean-team@mozilla.com>
License: MPL-2.0

Files: ./src/system.rs
Copyright: 2017 The Rust Project Developers
License: MIT

Files: debian/*
Copyright:
 2021 Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
 2021 Sylvestre Ledru <sylvestre@debian.org>
License: MPL-2.0

License: MPL-2.0
 Debian systems provide the MPL 2.0 in /usr/share/common-licenses/MPL-2.0

License: MIT
 Permission is hereby granted, free of charge, to any
 person obtaining a copy of this software and associated
 documentation files (the "Software"), to deal in the
 Software without restriction, including without
 limitation the rights to use, copy, modify, merge,
 publish, distribute, sublicense, and/or sell copies of
 the Software, and to permit persons to whom the Software
 is furnished to do so, subject to the following
 conditions:
 .
 The above copyright notice and this permission notice
 shall be included in all copies or substantial portions
 of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF
 ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
 SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
 IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.

