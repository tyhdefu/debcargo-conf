Source: rust-sequoia-octopus-librnp
Section: rust
Priority: optional
Build-Depends: debhelper (>= 12),
 python3:native,
 cargo:native,
 rustc:native,
 libstd-rust-dev,
 librust-anyhow-1+default-dev,
 librust-buffered-reader-1+default-dev,
 librust-chrono-0.4+default-dev,
 librust-configparser-2+default-dev,
 librust-csv-1+default-dev (>= 1.1-~~),
 librust-lazy-static-1+default-dev,
 librust-libc-0.2+default-dev,
 librust-libgit2-sys-0.12+default-dev,
 librust-num-cpus-1+default-dev,
 librust-rand-0.8+std-dev,
 librust-rand-0.8+std-rng-dev,
 librust-rand-distr-0.4-dev,
 librust-rusqlite-0.28+buildtime-bindgen-dev,
 librust-rusqlite-0.28+default-dev,
 librust-sequoia-autocrypt-0.24-dev,
 librust-sequoia-ipc-0.28-dev,
 librust-sequoia-net-0.25-dev,
 librust-sequoia-openpgp-1+default-dev (>= 1.6-~~),
 librust-sequoia-openpgp-1-dev (>= 1.6-~~),
 librust-sequoia-openpgp-mt-0.1+default-dev,
 librust-serde-1+default-dev,
 librust-serde-1+derive-dev,
 librust-serde-json-1+default-dev,
 librust-sha2-0.9+default-dev,
 librust-tempfile-3+default-dev,
 librust-thiserror-1+default-dev,
 librust-tokio-1+default-dev,
 librust-vergen-6+git-dev
Maintainer: Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
Uploaders:
 Daniel Kahn Gillmor <dkg@fifthhorseman.net>
Standards-Version: 4.5.1
Vcs-Git: https://salsa.debian.org/rust-team/debcargo-conf.git [src/sequoia-octopus-librnp]
Vcs-Browser: https://salsa.debian.org/rust-team/debcargo-conf/tree/master/src/sequoia-octopus-librnp
Homepage: https://sequoia-pgp.org/
Rules-Requires-Root: no

Package: libsequoia-octopus-librnp
Section: libs
Architecture: any
Enhances: thunderbird (>= 1:102~)
Suggests: thunderbird (>= 1:102~)
Conflicts: thunderbird (<< 1:102~)
Depends:
 ${misc:Depends},
 ${shlibs:Depends}
Description: Reimplementation of librnp for Thunderbird by Sequoia
 This package contains a dynamic library (shared object) with the same
 interface as librnp.so, at least the parts used by Thunderbird.  This
 implementation is built in Rust by the Sequoia OpenPGP project.
 .
 DO NOT USE this library as a generic librnp replacement, only use it
 with a recent version of Thunderbird.
 .
 When this package is installed, Thunderbird will use the library
 here, instead of the version shipped in the librnp0 package.
